﻿#nullable enable

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TcKs {
  public static class Helpers {
    public static Exception AssertAnyThrownException(Action action) => AssertAnyThrownException(action, null);

    public static Exception AssertAnyThrownException(Action action, string? message) {
      if (action is null) throw new ArgumentNullException(nameof(action));

      Exception thrownException = null!;
      try { action(); }
      catch (Exception exc) {
        thrownException = exc;
      }

      try {
        Assert.IsNotNull(thrownException, message ?? "Expected unhandled exception, but no exception was thrown.");
      }
      catch {
        throw;
      }

      return thrownException;
    }

    #region CheckSameCharacters
    public static void CheckSameCharacters(string a, RoChars b) {
      Assert.AreEqual(a.Length, b.Length);

      for (var i = 0; i < a.Length; i++) {
        Assert.AreEqual(a[i], b[i], $"Index - {i}");
      }
    }

    public static void CheckSameCharacters(string a, Span<char> b) {
      Assert.AreEqual(a.Length, b.Length);

      for (var i = 0; i < a.Length; i++) {
        Assert.AreEqual(a[i], b[i], $"Index - {i}");
      }
    }

    public static void CheckSameCharacters(RoChars a, Span<char> b) {
      Assert.AreEqual(a.Length, b.Length);

      for (var i = 0; i < a.Length; i++) {
        Assert.AreEqual(a[i], b[i], $"Index - {i}");
      }
    }

    public static void CheckSameCharacters(RoChars a, ReadOnlySpan<char> b) {
      Assert.AreEqual(a.Length, b.Length);

      for (var i = 0; i < a.Length; i++) {
        Assert.AreEqual(a[i], b[i], $"Index - {i}");
      }
    }

    public static void CheckSameCharacters(RoChars a, Memory<char> b) => CheckSameCharacters(a, b.Span);

    public static void CheckSameCharacters(RoChars a, ReadOnlyMemory<char> b) => CheckSameCharacters(a, b.Span);

    public static void CheckSameCharacters(string a, ReadOnlySpan<char> b) {
      Assert.AreEqual(a.Length, b.Length);

      for (var i = 0; i < a.Length; i++) {
        Assert.AreEqual(a[i], b[i], $"Index - {i}");
      }
    }

    public static void CheckSameCharacters(string a, ReadOnlyMemory<char> b) {
      Assert.AreEqual(a.Length, b.Length);

      var bSpan = b.Span;

      for (var i = 0; i < a.Length; i++) {
        Assert.AreEqual(a[i], bSpan[i], $"Index - {i}");
      }
    }
    #endregion CheckSameCharacters

    #region CheckCreatedStrSpan
    public static void CheckCreatedStrSpan(Func<string, RoChars> factory) {
      {
        var span = factory("");

        Assert.IsTrue(span.IsEmpty);
        CheckSameCharacters("", span);
      }

      {
        var span = factory(" ");

        Assert.IsFalse(span.IsEmpty);
        CheckSameCharacters(" ", span);
      }

      {
        var span = factory("hello world!");

        Assert.IsFalse(span.IsEmpty);
        CheckSameCharacters("hello world!", span);
      }
    }

    public static void CheckCreatedStrSpan(Func<string, int, RoChars> factory) {
      {
        var span = factory("", 0);

        Assert.IsTrue(span.IsEmpty);
        Assert.AreEqual(0, span.Offset);
        CheckSameCharacters("", span);
      }

      {
        var span = factory(" ", 0);

        Assert.IsFalse(span.IsEmpty);
        Assert.AreEqual(0, span.Offset);
        CheckSameCharacters(" ", span);
      }

      {
        var span = factory(" ", 1);

        Assert.IsTrue(span.IsEmpty);
        Assert.AreEqual(1, span.Offset);
        CheckSameCharacters("", span);
      }

      {
        var span = factory("hello world!", 0);

        Assert.IsFalse(span.IsEmpty);
        Assert.AreEqual(0, span.Offset);
        CheckSameCharacters("hello world!", span);
      }

      {
        var span = factory("hello world!", 1);

        Assert.IsFalse(span.IsEmpty);
        Assert.AreEqual(1, span.Offset);
        CheckSameCharacters("ello world!", span);
      }

      {
        var span = factory("hello world!", 2);

        Assert.IsFalse(span.IsEmpty);
        Assert.AreEqual(2, span.Offset);
        CheckSameCharacters("llo world!", span);
      }

      {
        var span = factory("hello world!", 11);

        Assert.IsFalse(span.IsEmpty);
        Assert.AreEqual(11, span.Offset);
        CheckSameCharacters("!", span);
      }

      {
        var span = factory("hello world!", 12);

        Assert.IsTrue(span.IsEmpty);
        Assert.AreEqual(12, span.Offset);
        CheckSameCharacters("", span);
      }
    }

    public static void CheckCreatedStrSpan(Func<string, int, int, RoChars> factory) {
      {
        var span = factory("", 0, 0);

        Assert.IsTrue(span.IsEmpty);
        Assert.AreEqual(0, span.Offset);
        CheckSameCharacters("", span);
      }

      void CheckSubstring(string orig, int offset, int length) {
        var span = factory(orig, offset, length);

        Assert.AreEqual(offset, span.Offset);
        Assert.AreEqual(length, span.Length);

        var substr = orig.Substring(offset, length);
        CheckSameCharacters(substr, span);

        var source = RoChars.GetSource(span);
        CheckSameCharacters(orig, source);
      }

      CheckSubstring(" ", 0, 0);
      CheckSubstring(" ", 0, 1);
      CheckSubstring(" ", 1, 0);

      CheckSubstring("ab", 0, 0);
      CheckSubstring("ab", 0, 1);
      CheckSubstring("ab", 0, 2);
      CheckSubstring("ab", 1, 0);
      CheckSubstring("ab", 1, 1);
      CheckSubstring("ab", 2, 0);

      CheckSubstring("hello world!", 0, 0);
      CheckSubstring("hello world!", 0, 1);
      CheckSubstring("hello world!", 0, 6);
      CheckSubstring("hello world!", 0, 11);
      CheckSubstring("hello world!", 0, 12);
      CheckSubstring("hello world!", 4, 0);
      CheckSubstring("hello world!", 4, 1);
      CheckSubstring("hello world!", 4, 2);
      CheckSubstring("hello world!", 4, 3);
      CheckSubstring("hello world!", 4, 8);
      CheckSubstring("hello world!", 11, 0);
      CheckSubstring("hello world!", 11, 1);
      CheckSubstring("hello world!", 12, 0);
    }
    #endregion CheckCreatedStrSpan

    #region CheckStrSpanFactoryThrows
    public static void CheckStrSpanFactoryThrows(Func<string?, int, RoChars> factory) {
      CheckStrSpanFactoryThrows(factory, true);
    }

    public static void CheckStrSpanFactoryThrows(Func<string?, int, RoChars> factory, bool allowNull) {
      void CheckThrow(string? source, int offset) {
        AssertAnyThrownException(() => factory(source, offset), $"offset: {offset}, source: {source}");
      }

      if (allowNull) {
        CheckThrow(null, 1);
        CheckThrow(null, 2);
        CheckThrow(null, 3);
        CheckThrow(null, int.MaxValue);
        CheckThrow(null, -1);
        CheckThrow(null, -2);
        CheckThrow(null, -3);
        CheckThrow(null, -int.MaxValue);
      }

      CheckThrow("", 1);
      CheckThrow("", 2);
      CheckThrow("", 3);
      CheckThrow("", int.MaxValue);
      CheckThrow("", -1);
      CheckThrow("", -2);
      CheckThrow("", -3);
      CheckThrow("", -int.MaxValue);

      CheckThrow(" ", 2);
      CheckThrow(" ", 3);
      CheckThrow(" ", int.MaxValue);
      CheckThrow(" ", -1);
      CheckThrow(" ", -2);
      CheckThrow(" ", -3);
      CheckThrow(" ", -int.MaxValue);

      CheckThrow("hello world!", 13);
      CheckThrow("hello world!", 14);
      CheckThrow("hello world!", 15);
      CheckThrow("hello world!", int.MaxValue);
      CheckThrow("hello world!", -1);
      CheckThrow("hello world!", -2);
      CheckThrow("hello world!", -3);
      CheckThrow("hello world!", -int.MaxValue);
    }

    public static void CheckStrSpanFactoryThrows(Func<string?, int, int, RoChars> factory) {
      CheckStrSpanFactoryThrows(factory, true);
    }

    public static void CheckStrSpanFactoryThrows(Func<string?, int, int, RoChars> factory, bool allowNull) {
      void CheckThrow(string? source, int offset, int length) {
        AssertAnyThrownException(() => factory(source, offset, length), $"offset: {offset}, length: {length}, source: {source}");
      }

      var baseLengths = new int[] { 0, 1, 2, 3, int.MaxValue, -1, -2, -3, int.MinValue };

      if (allowNull) {
        foreach (var len in baseLengths) {
          if (len != 0) {
            CheckThrow(null, 0, len);
          }

          CheckThrow(null, 1, len);
          CheckThrow(null, 2, len);
          CheckThrow(null, 3, len);
          CheckThrow(null, int.MaxValue, len);
          CheckThrow(null, -1, len);
          CheckThrow(null, -2, len);
          CheckThrow(null, -3, len);
          CheckThrow(null, -int.MaxValue, len);
        }
      }

      foreach (var len in baseLengths) {
        CheckThrow("", 1, len);
        CheckThrow("", 2, len);
        CheckThrow("", 3, len);
        CheckThrow("", int.MaxValue, len);
        CheckThrow("", -1, len);
        CheckThrow("", -2, len);
        CheckThrow("", -3, len);
        CheckThrow("", -int.MaxValue, len);
      }

      CheckThrow(" ", 0, 2);
      CheckThrow(" ", 0, 3);
      CheckThrow(" ", 0, int.MaxValue);
      CheckThrow(" ", 0, -1);
      CheckThrow(" ", 0, -2);
      CheckThrow(" ", 0, -3);
      CheckThrow(" ", 0, int.MinValue);
      foreach (var len in baseLengths) {
        CheckThrow(" ", 2, len);
        CheckThrow(" ", 3, len);
        CheckThrow(" ", int.MaxValue, len);
        CheckThrow(" ", -1, len);
        CheckThrow(" ", -2, len);
        CheckThrow(" ", -3, len);
        CheckThrow(" ", -int.MaxValue, len);
      }
      
      CheckThrow("hello world!", 0, 13);
      CheckThrow("hello world!", 0, 14);
      CheckThrow("hello world!", 0, 15);
      CheckThrow("hello world!", 0, int.MaxValue);
      CheckThrow("hello world!", 0, -1);
      CheckThrow("hello world!", 0, -2);
      CheckThrow("hello world!", 0, -3);
      CheckThrow("hello world!", 0, int.MinValue);
      foreach (var len in baseLengths) {
        CheckThrow("hello world!", 13, len);
        CheckThrow("hello world!", 14, len);
        CheckThrow("hello world!", 15, len);
        CheckThrow("hello world!", int.MaxValue, len);
        CheckThrow("hello world!", -1, len);
        CheckThrow("hello world!", -2, len);
        CheckThrow("hello world!", -3, len);
        CheckThrow("hello world!", -int.MaxValue, len);
      }
    }
    #endregion CheckStrSpanFactoryThrows
  }
}
