using Microsoft.VisualStudio.TestTools.UnitTesting;

using static TcKs.Helpers;

namespace TcKs.RoCharsTests {
  [TestClass]
  public class ToStringTests {
    [TestMethod]
    public void WorksWithEmptyString() {
      var strSpan = RoChars.From(string.Empty);
      var str = strSpan.ToString();
      Assert.AreEqual(str, strSpan.ToString());

      Assert.AreEqual(0, str.Length);
      Assert.IsTrue(RoChars.LazyFields.IsSubStringLoaded(ref strSpan));
    }

    [TestMethod]
    public void WorksWithFullStringString() {
      var origStr = "hello world";
      var strSpan = RoChars.From(origStr);
      var str = strSpan.ToString();
      Assert.AreEqual(str, strSpan.ToString());

      Assert.AreEqual(origStr, str);
      Assert.IsTrue(RoChars.LazyFields.IsSubStringLoaded(ref strSpan));
    }

    [DataTestMethod,
      DataRow(0, 1), DataRow(0, 2), DataRow(0, 3),
      DataRow(1, 1), DataRow(1, 2), DataRow(1, 3),
      DataRow(8, 1), DataRow(8, 2), DataRow(8, 3)
    ]
    public void WorksWithPartialStringString(int offset, int length) {
      var origStr = "hello world";
      var subStr = origStr.Substring(offset, length);
      var strSpan = RoChars.From(origStr, offset, length);
      var str = strSpan.ToString();
      Assert.AreEqual(str, strSpan.ToString());

      Assert.AreEqual(subStr, str);
      Assert.IsTrue(RoChars.LazyFields.IsSubStringLoaded(ref strSpan));
    }
  }
}