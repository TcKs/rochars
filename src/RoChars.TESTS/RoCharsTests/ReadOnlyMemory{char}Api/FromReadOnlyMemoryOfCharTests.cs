#nullable enable

using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TcKs.RoCharsTests.StringApi {
  [TestClass]
  public class FromReadOnlyMemoryOfCharTests {
    private RoChars FromString(string str) {
      return RoChars.From(str.AsMemory());
    }

    private RoChars FromString(string str, int offset) {
      return RoChars.From(str.AsMemory(), offset);
    }

    private RoChars FromString(string str, int offset, int length) {
      return RoChars.From(str.AsMemory(), offset, length);
    }

    [TestMethod]
    public void FromStringWorks() {
      Helpers.CheckCreatedStrSpan(str => FromString(str!));
    }

    [TestMethod]
    public void FromStringAndIntWorks() {
      Helpers.CheckCreatedStrSpan((str, offset) => FromString(str!, offset));
    }

    [TestMethod]
    public void FromStringAndIntAndIntWorks() {
      Helpers.CheckCreatedStrSpan((str, offset, length) => FromString(str!, offset, length));
    }

    [TestMethod]
    public void FromStringAndIntThrows() {
      Helpers.CheckStrSpanFactoryThrows((str, offset) => FromString(str!, offset));
    }

    [TestMethod]
    public void FromStringAndIntAndIntThrows() {
      Helpers.CheckStrSpanFactoryThrows((str, offset, length) => FromString(str!, offset, length));
    }
  }
}