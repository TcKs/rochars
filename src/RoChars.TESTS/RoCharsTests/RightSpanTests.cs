using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TcKs.StrSpan_ {
  [TestClass]
  public class RightSpanTests {
    [DataTestMethod, DataRow(""), DataRow(" "), DataRow("hello"), DataRow("lorem ipsum")]
    public void RightSpanWorks(string str) {
      var span = RoChars.From(str);

      Assert.IsTrue(span.LeftSpan(0).IsEmpty);

      if (str is null) return;

      for (var i = 0; i < str.Length; i++) {
        var subStr = str.Substring(str.Length - i, i);
        var leftSpan = span.RightSpan(i);

        Assert.AreEqual(subStr.Length, leftSpan.Length);
        Helpers.CheckSameCharacters(subStr, leftSpan);
      }
    }
  }
}