using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TcKs.StrSpan_ {
  [TestClass]
  public class LazyFieldsTests {
    [DataTestMethod, DataRow(""), DataRow("hello world")]
    public void GetLazyFieldsByValWorks(string input) {
      var span = RoChars.From(input);

      RoChars.GetLazyFields(span);
    }

    [DataTestMethod, DataRow(""), DataRow("hello world")]
    public void GetLazyFieldsByRefWorks(string input) {
      var span = RoChars.From(input);

      RoChars.GetLazyFields(ref span);
    }

    [DataTestMethod, DataRow(""), DataRow("hello world")]
    public void ClearLazyFieldsWorks(string input) {
      var span = RoChars.From(input);

      span.GetHashCode();
      span.ToString();

      {
        var fields = RoChars.GetLazyFields(span);
        Assert.IsTrue(RoChars.LazyFields.IsHashCodeLoaded(ref fields));
        if (!string.IsNullOrEmpty(input)) {
          Assert.IsTrue(RoChars.LazyFields.IsSubStringLoaded(ref fields));
        }
      }

      RoChars.ClearLazyFields(ref span);
      {
        var fields = RoChars.GetLazyFields(span);
        Assert.IsFalse(RoChars.LazyFields.IsHashCodeLoaded(ref fields));
        Assert.IsFalse(RoChars.LazyFields.IsSubStringLoaded(ref fields));
      }
    }
  }
}