using Microsoft.VisualStudio.TestTools.UnitTesting;

using static TcKs.Helpers;

namespace TcKs.RoCharsTests {
  [TestClass]
  public class ToMemoryTests {
    [TestMethod]
    public void WorksWithEmptyString() {
      var strSpan = RoChars.From(string.Empty);
      var memo = strSpan.ToMemory();

      Assert.AreEqual(0, memo.Length);
      Assert.IsTrue(memo.IsEmpty);
    }

    [TestMethod]
    public void WorksWithFullStringString() {
      var str = "hello world";
      var strSpan = RoChars.From(str);
      var memo = strSpan.ToMemory();

      Assert.AreEqual(str.Length, memo.Length);
      Assert.IsFalse(memo.IsEmpty);

      CheckSameCharacters(str, memo);
    }

    [DataTestMethod,
      DataRow(0, 1), DataRow(0, 2), DataRow(0, 3),
      DataRow(1, 1), DataRow(1, 2), DataRow(1, 3),
      DataRow(8, 1), DataRow(8, 2), DataRow(8, 3)
    ]
    public void WorksWithPartialStringString(int offset, int length) {
      var str = "hello world";
      var strSpan = RoChars.From(str, offset, length);
      var memo = strSpan.ToMemory();

      CheckSameCharacters(strSpan, memo);
    }
  }
}