#nullable enable

using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TcKs.RoCharsTests.StringApi {
  [TestClass]
  public class EqualsStringTests {
    [DataTestMethod]
    [DataRow(true, "", ""), DataRow(true, "hello", "hello"), DataRow(false, "", "hello")]
    public void EqualsWithStrSpanAndStringWorks(bool expectedResult, string aStr, string bStr) {
      void RunAsserts(RoChars a, string? b) {
        Assert.AreEqual(expectedResult, RoChars.Equals(a, b));
        Assert.AreEqual(expectedResult, RoChars.Equals(ref a, b));
        Assert.AreEqual(expectedResult, RoChars.Equals(b, a));
        Assert.AreEqual(expectedResult, RoChars.Equals(b, ref a));

        Assert.AreEqual(expectedResult, a.Equals(b));
        Assert.AreEqual(expectedResult, a.Equals((object?)b));

        Assert.AreEqual(expectedResult, a == b);
        Assert.AreEqual(!expectedResult, a != b);

        Assert.AreEqual(expectedResult, b == a);
        Assert.AreEqual(!expectedResult, b != a);
      }

      var aSpan = RoChars.From(aStr);
      var bSpan = RoChars.From(bStr);

      RunAsserts(aSpan, bStr);
      RunAsserts(bSpan, aStr);
    }

    [DataTestMethod]
    [DataRow(true, "", ""), DataRow(true, "hello", "hello"), DataRow(false, "", "hello")]
    public void EqualsWithNullableStrSpanAndStringWorks(bool expectedResult, string aStr, string bStr) {
      static void RunAsserts(bool expectedResult, RoChars? a, string? b) {
        Assert.AreEqual(expectedResult, RoChars.Equals(a, b));
        Assert.AreEqual(expectedResult, RoChars.Equals(ref a, b));
        Assert.AreEqual(expectedResult, RoChars.Equals(b, a));
        Assert.AreEqual(expectedResult, RoChars.Equals(b, ref a));

        if (a is not null) {
          Assert.AreEqual(expectedResult, a.Equals(b));
        }

        Assert.AreEqual(expectedResult, a == b);
        Assert.AreEqual(!expectedResult, a != b);

        Assert.AreEqual(expectedResult, b == a);
        Assert.AreEqual(!expectedResult, b != a);
      }

      var aSpan = RoChars.From(aStr);
      var bSpan = RoChars.From(bStr);

      RunAsserts(expectedResult, aSpan, bStr);
      RunAsserts(expectedResult, bSpan, aStr);

      RunAsserts(false, aSpan, null);
      RunAsserts(false, bSpan, null);
    }

    [TestMethod]
    public void EqualsWithNullStrSpanAdnStringWorks() {
      string? nullString = null;
      string? emptyString = "";

      RoChars? nullSpan = null;
      RoChars emptySpan = RoChars.Empty;

      Assert.IsTrue(RoChars.Equals(nullString, nullSpan));
      Assert.IsTrue(RoChars.Equals(nullSpan, nullString));
      Assert.IsTrue(RoChars.Equals(nullString, ref nullSpan));
      Assert.IsTrue(RoChars.Equals(ref nullSpan, nullString));

      Assert.IsFalse(RoChars.Equals(nullString, emptySpan));
      Assert.IsFalse(RoChars.Equals(emptySpan, nullString));
      Assert.IsFalse(RoChars.Equals(nullString, ref emptySpan));
      Assert.IsFalse(RoChars.Equals(ref emptySpan, nullString));

      Assert.IsFalse(RoChars.Equals(emptyString, nullSpan));
      Assert.IsFalse(RoChars.Equals(nullSpan, emptyString));
      Assert.IsFalse(RoChars.Equals(emptyString, ref nullSpan));
      Assert.IsFalse(RoChars.Equals(ref nullSpan, emptyString));
    }
  }
}