#nullable enable

using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TcKs.RoCharsTests.StringApi {
  [TestClass]
  public class EqualsSpanOfCharTests {
    [DataTestMethod]
    [DataRow(true, "", ""), DataRow(true, "hello", "hello")]
    [DataRow(false, "", "hello")]
    public void EqualsWithStrSpanAndSpanOfCharWorks(bool expectedResult, string aStr, string bStr) {
      void RunAsserts(RoChars a, string? b) {
        var bCharSpan = b!.ToCharArray().AsSpan();

        Assert.AreEqual(expectedResult, RoChars.Equals(a, bCharSpan));
        Assert.AreEqual(expectedResult, RoChars.Equals(ref a, bCharSpan));
        Assert.AreEqual(expectedResult, RoChars.Equals(bCharSpan, a));
        Assert.AreEqual(expectedResult, RoChars.Equals(bCharSpan, ref a));

        Assert.AreEqual(expectedResult, a.Equals(bCharSpan));

        Assert.AreEqual(expectedResult, a == bCharSpan);
        Assert.AreEqual(!expectedResult, a != bCharSpan);
        Assert.AreEqual(expectedResult, bCharSpan == a);
        Assert.AreEqual(!expectedResult, bCharSpan != a);
      }

      var aSpan = RoChars.From(aStr);
      var bSpan = RoChars.From(bStr);

      RunAsserts(aSpan, bStr);
      RunAsserts(bSpan, aStr);
    }

    [DataTestMethod,]
    [DataRow(true, "", ""), DataRow(true, "hello", "hello"), DataRow(false, "", "hello")]
    public void EqualsWithNullableStrSpanAndSpanOfCharWorks(bool expectedResult, string aStr, string bStr) {
      static void RunAsserts(bool expectedResult, RoChars? a, string bSource) {
        var b = bSource!.ToCharArray().AsSpan();

        Assert.AreEqual(expectedResult, RoChars.Equals(a, b));
        Assert.AreEqual(expectedResult, RoChars.Equals(ref a, b));
        Assert.AreEqual(expectedResult, RoChars.Equals(b, a));
        Assert.AreEqual(expectedResult, RoChars.Equals(b, ref a));

        Assert.AreEqual(expectedResult, a == b);
        Assert.AreEqual(!expectedResult, a != b);
        Assert.AreEqual(expectedResult, b == a);
        Assert.AreEqual(!expectedResult, b != a);
      }

      var aSpan = RoChars.From(aStr);
      var bSpan = RoChars.From(bStr);

      RunAsserts(expectedResult, aSpan, bStr);
      RunAsserts(false, null, bStr);
 
      RunAsserts(expectedResult, bSpan, aStr);
      RunAsserts(false, null, aStr);
    }
  }
}