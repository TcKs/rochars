﻿/// *************************************************************************************************************
/// This is model file, with support for ref-readonly structs.
/// The target ref-readonly struct is aliased as "TOther" and is used in whole code.
/// Keep in mind, that this file will be used as main template for other ref-readonly struct support files.
/// *************************************************************************************************************

#nullable enable

using TOther = System.Span<char>;

using System;

namespace TcKs {
  partial struct RoChars {
    #region Static factory methods
    /// <summary>
    /// Creates new <see cref="RoChars"/> from string.
    /// </summary>
    /// <param name="source"></param>
    /// <returns></returns>
    public static RoChars From(TOther source) => source.IsEmpty ? Empty : From(source.ToReadOnlyMemory());

    /// <summary>
    /// Creates new <see cref="RoChars"/> from string and offset.
    /// </summary>
    /// <param name="source"></param>
    /// <param name="offset"></param>
    /// <returns></returns>
    public static RoChars From(TOther source, int offset) => From(source.ToReadOnlyMemory(), offset);

    /// <summary>
    /// Creates new <see cref="RoChars"/> from string and offset.
    /// </summary>
    /// <param name="source"></param>
    /// <param name="offset"></param>
    /// <returns></returns>
    public static RoChars From(TOther source, int offset, int length) {
      if (offset < 0) throw MakeExceptionForOffsetIsLessThanZero(offset);
      if (length < 0) throw MakeExceptionForLengthIsLessThanZero(length);

      var srcLen = source.Length;
      var maxLen = srcLen - offset;

      if (length > maxLen) throw MakeExceptionForLengthIsGreaterThanMaximumLength(length, maxLen);

      return From(source.ToReadOnlyMemory(), offset, length);
    }
    #endregion Static factory methods

    #region Equality (string)
    public bool Equals(TOther other) => Equals(ref this, other);

    public static bool Equals(TOther a, RoChars b) => Equals(From(a), ref b);
    public static bool Equals(TOther a, ref RoChars b) => Equals(From(a), ref b);

    public static bool Equals(RoChars a, TOther b) => Equals(ref a, From(b));
    public static bool Equals(ref RoChars a, TOther b) => Equals(ref a, From(b));

    public static bool Equals(TOther a, RoChars? b) => Equals(From(a), ref b);
    public static bool Equals(TOther a, ref RoChars? b) => Equals(From(a), ref b);

    public static bool Equals(RoChars? a, TOther b) => Equals(a, From(b));
    public static bool Equals(ref RoChars? a, TOther b) => Equals(a, From(b));

    public static bool operator ==(TOther a, RoChars b) => Equals(a, b);
    public static bool operator !=(TOther a, RoChars b) => !Equals(a, b);

    public static bool operator ==(RoChars a, TOther b) => Equals(a, b);
    public static bool operator !=(RoChars a, TOther b) => !Equals(a, b);

    public static bool operator ==(TOther a, RoChars? b) => Equals(a, b);
    public static bool operator !=(TOther a, RoChars? b) => !Equals(a, b);

    public static bool operator ==(RoChars? a, TOther b) => Equals(a, b);
    public static bool operator !=(RoChars? a, TOther b) => !Equals(a, b);
    #endregion Equality (string)
  }
}
