﻿/// *************************************************************************************************************
/// This is file is copied from main template - StrsSpan.Memory(char).main.cs.
/// Do not change this file alone. All fixes or improvements should be made in main tempalte file,
/// and applied to the rest of derived files.
/// *************************************************************************************************************

#nullable enable

using TOther = System.ReadOnlyMemory<char>;

using System;

namespace TcKs {
  partial struct RoChars {
    #region Static factory methods
    /// <summary>
    /// Creates new <see cref="RoChars"/> from string.
    /// </summary>
    /// <param name="source"></param>
    /// <returns></returns>
    public static RoChars From(TOther source) => source.IsEmpty ? Empty : new RoChars(source);

    /// <summary>
    /// Creates new <see cref="RoChars"/> from string and offset.
    /// </summary>
    /// <param name="source"></param>
    /// <param name="offset"></param>
    /// <returns></returns>
    public static RoChars From(TOther source, int offset) {
      if (offset < 0) throw MakeExceptionForOffsetIsLessThanZero(offset);

      var srcLen = source.Length;
      var len = srcLen - offset;

      return len < 0 ? throw MakeExceptionForOffsetIsGreaterThanLengthOfSource(source, offset)
            : new RoChars(source, offset, len);
    }

    /// <summary>
    /// Creates new <see cref="RoChars"/> from string and offset.
    /// </summary>
    /// <param name="source"></param>
    /// <param name="offset"></param>
    /// <returns></returns>
    public static RoChars From(TOther source, int offset, int length) {
      if (offset < 0) throw MakeExceptionForOffsetIsLessThanZero(offset);
      if (length < 0) throw MakeExceptionForLengthIsLessThanZero(length);

      var srcLen = source.Length;
      var maxLen = srcLen - offset;

      if (length > maxLen) throw MakeExceptionForLengthIsGreaterThanMaximumLength(length, maxLen);

      return new RoChars(source, offset, length);
    }
    #endregion Static factory methods

    #region Equality (string)
    public bool Equals(TOther other) => Equals(ref this, other);

    public static bool Equals(TOther a, RoChars b) => Equals(a, ref b);
    public static bool Equals(TOther a, ref RoChars b) => Equals(From(a), ref b);

    public static bool Equals(RoChars a, TOther b) => Equals(ref a, b);
    public static bool Equals(ref RoChars a, TOther b) => Equals(ref a, From(b));

    public static bool Equals(TOther a, RoChars? b) => Equals(a, ref b);
    public static bool Equals(TOther a, ref RoChars? b) => Equals(From(a), ref b);

    public static bool Equals(RoChars? a, TOther b) => Equals(ref a, b);
    public static bool Equals(ref RoChars? a, TOther b) => Equals(a, From(b));

    public static bool operator ==(TOther a, RoChars b) => Equals(a, b);
    public static bool operator !=(TOther a, RoChars b) => !Equals(a, b);

    public static bool operator ==(RoChars a, TOther b) => Equals(a, b);
    public static bool operator !=(RoChars a, TOther b) => !Equals(a, b);

    public static bool operator ==(TOther a, RoChars? b) => Equals(a, b);
    public static bool operator !=(TOther a, RoChars? b) => !Equals(a, b);

    public static bool operator ==(RoChars? a, TOther b) => Equals(a, b);
    public static bool operator !=(RoChars? a, TOther b) => !Equals(a, b);
    #endregion Equality (string)
  }
}
