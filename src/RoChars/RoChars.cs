﻿#nullable enable

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcKs {
  public partial struct RoChars : IEquatable<RoChars>, IEquatable<RoChars?> {
    #region Empty
    private static readonly RoChars empty = MakeEmpty();
    public static RoChars Empty => empty;

    public static RoChars MakeEmpty() {
      var result = default(RoChars);
      LazyFields.PopulateAll(ref result);

      return result;
    }
    #endregion Empty

    #region Source
    /// <summary>
    /// The source string.
    /// </summary>
    private readonly ReadOnlyMemory<char> source;

    /// <summary>
    /// Returns source string of the span.
    /// </summary>
    /// <param name="input">Span from where you want source string.</param>
    /// <returns></returns>
    public static ReadOnlyMemory<char> GetSource(RoChars input) => input.source;

    /// <summary>
    /// Returns source string of the span.
    /// </summary>
    /// <param name="input">Span from where you want source string.</param>
    /// <returns></returns>
    public static ReadOnlyMemory<char> GetSource(ref RoChars input) => input.source;
    #endregion Source

    #region Offset
    private readonly int offset;

    /// <summary>
    /// Offset from where this span starts.
    /// </summary>
    public int Offset => offset;
    #endregion Offset

    #region Length
    private readonly int length;

    /// <summary>
    /// Length of this span.
    /// </summary>
    public int Length => length;
    #endregion Length

    #region IsEmpty
    public bool IsEmpty => length < 1;
    #endregion IsEmpty

    #region LazyFields
    private LazyFields lazy;

    public static LazyFields GetLazyFields(RoChars input) => input.lazy;
    public static LazyFields GetLazyFields(ref RoChars input) => input.lazy;

    public static void ClearLazyFields(ref RoChars input) => LazyFields.ClearAll(ref input.lazy);
    #endregion LazyFields

    #region Index access
    public char this[int index] => this.CharAt(index);

    public RoChars this[int index, int length] => this.SubSpan(index, length);
    #endregion Index access

    #region Constructors
    /// <summary>
    /// Constructor for full span of string.
    /// </summary>
    /// <param name="source"><see cref="GetSource(RoChars)"/></param>
    private RoChars(ReadOnlyMemory<char> source) {
      this.lazy = default;

      this.source = source;
      this.offset = 0;
      this.length = source!.Length;
    }

    /// <summary>
    /// Privat constructor.
    /// Do not chech input arguments.
    /// </summary>
    /// <param name="source"><see cref="GetSource(RoChars)"/></param>
    /// <param name="offset"><see cref="Offset"/></param>
    /// <param name="length"><see cref="Length"/></param>
    private RoChars(ReadOnlyMemory<char> source, int offset, int length) {
      this.lazy = default;

      this.source = source;
      this.offset = offset;
      this.length = length;
    }
    #endregion Constructors

    #region Exception factory methods
    [ExcludeFromCodeCoverage]
    private static Exception MakeExceptionForOffsetIsLessThanZero(int offset) {
      return new ArgumentOutOfRangeException(nameof(offset), $"Offset ({offset}) is less than zero.");
    }

    [ExcludeFromCodeCoverage]
    private static Exception MakeExceptionForLengthIsLessThanZero(int length) {
      return new ArgumentOutOfRangeException(nameof(length), $"Length ({length}) is less than zero.");
    }

    [ExcludeFromCodeCoverage]
    private static Exception MakeExceptionForOffsetIsGreaterThanLengthOfSource(ReadOnlyMemory<char> source, int offset) {
      return new ArgumentOutOfRangeException(nameof(offset), $"Offset ({offset}) is greater than length of source ({source.Length}).");
    }

    [ExcludeFromCodeCoverage]
    private static Exception MakeExceptionForOffsetIsGreaterThanLengthOfSource(int offset, int length) {
      return new ArgumentOutOfRangeException(nameof(offset), $"Offset ({offset}) is greater than length of source ({length}).");
    }

    [ExcludeFromCodeCoverage]
    private static Exception MakeExceptionForLengthIsGreaterThanMaximumLength(int length, int maximumLength) {
      throw new ArgumentOutOfRangeException(nameof(length), $"Length ({length}) is greater than maximum available length ({maximumLength}).");
    }
    #endregion Exception factory methods

    #region CharAt
    public char CharAt(int index) {
      if (index >= this.length) throw new ArgumentOutOfRangeException(nameof(index));

      return this.source.Span[this.offset + index];
    }
    #endregion CharAt

    #region (Sub/*)Span methods
    public RoChars SubSpan(int index, int length) {
      if (index > this.length) throw new ArgumentOutOfRangeException(nameof(index));

      var maxLen = this.length - index;
      if (length > maxLen) throw new ArgumentOutOfRangeException(nameof(length));

      return From(this.source, this.offset + index, length);
    }

    public RoChars LeftSpan(int length) => this.SubSpan(0, length);

    public RoChars RightSpan(int length) => this.SubSpan(this.length - length, length);
    #endregion (Sub/*)Span methods

    #region Prev/Next methods
    public RoChars Prev() => From(this.source, 0, this.offset);

    public RoChars Next() => From(this.source, this.offset + this.length);
    #endregion Prev/Next methods

    #region To*** methods
    public ReadOnlySpan<char> ToSpan() => IsEmpty ? default : this.source.Slice(this.offset, this.length).Span;

    public ReadOnlyMemory<char> ToMemory() => IsEmpty ? default : this.source.Slice(this.offset, this.length);

    public override string? ToString() => LazyFields.GetSubString(ref this);

    public string? ComputeString() => IsEmpty ? string.Empty : this.source.Slice(this.offset, this.length).ToString();
    #endregion To*** methods

    #region HashCode
    public HashCode ComputeHashCode() {
      if (this.IsEmpty) return default;

      var result = new HashCode();

      var src = this.source.Span;
      var off = this.offset;
      var len = this.length;
      for (var i = 0; i < len; i++) {
        var ch = src[i + off];
        result.Add(ch);
      }

      return result;
    }

    public override int GetHashCode() => LazyFields.GetHashCode(ref this);
    #endregion HashCode

    #region Equality (RoChars)
    public bool Equals(RoChars other) => Equals(ref this, ref other);
    public bool Equals(ref RoChars other) => Equals(ref this, ref other);

    public static bool Equals(RoChars a, RoChars b) => Equals(ref a, ref b);
    public static bool Equals(RoChars a, ref RoChars b) => Equals(ref a, ref b);
    public static bool Equals(ref RoChars a, RoChars b) => Equals(ref a, ref b);
    public static bool Equals(ref RoChars a, ref RoChars b) {
      var aLen = a.length;
      var bLen = b.length;

      if (aLen != bLen) return false;

      var aHc = a.GetHashCode();
      var bHc = b.GetHashCode();

      if (aHc != bHc) return false;

      var aSrc = a.source.Span;
      var aOff = a.offset;

      var bSrc = b.source.Span;
      var bOff = b.offset;

      for (var i = 0; i < aLen; i++) {
        var aCh = aSrc[aOff + i];
        var bCh = bSrc[bOff + i];

        if (aCh != bCh) return false;
      }

      return true;
    }

    public static bool operator ==(RoChars a, RoChars b) => Equals(ref a, ref b);
    public static bool operator !=(RoChars a, RoChars b) => !Equals(ref a, ref b);
    #endregion Equality (RoChars)

    #region Equality (RoChars?)
    public bool Equals(RoChars? other) => Equals(ref this, ref other);
    public bool Equals(ref RoChars? other) => Equals(ref this, ref other);

    public static bool Equals(RoChars? a, ref RoChars b) => Equals(ref a, ref b);
    public static bool Equals(ref RoChars? a,RoChars b) => Equals(ref a, ref b);
    public static bool Equals(ref RoChars? a, ref RoChars b) => a.HasValue && Equals(a.Value, ref b);

    public static bool Equals(RoChars a, ref RoChars? b) => Equals(ref a, ref b);
    public static bool Equals(ref RoChars a, RoChars? b) => Equals(ref a, ref b);
    public static bool Equals(ref RoChars a, ref RoChars? b) => Equals(ref b, ref a);

    public static bool Equals(RoChars? a, RoChars? b) => Equals(ref a, ref b);
    public static bool Equals(RoChars? a, ref RoChars? b) => Equals(ref a, ref b);
    public static bool Equals(ref RoChars? a, RoChars? b) => Equals(ref a, ref b);
    public static bool Equals(ref RoChars? a, ref RoChars? b) {
      var aHasValue = a.HasValue;
      var bHasValue = b.HasValue;

      if (aHasValue) {
        return bHasValue && Equals(a!.Value, b!.Value);
      }

      return !bHasValue;
    }

    public static bool operator ==(RoChars? a, RoChars b) => Equals(ref a, ref b);
    public static bool operator !=(RoChars? a, RoChars b) => !Equals(ref a, ref b);

    public static bool operator ==(RoChars a, RoChars? b) => Equals(ref a, ref b);
    public static bool operator !=(RoChars a, RoChars? b) => !Equals(ref a, ref b);

    public static bool operator ==(RoChars? a, RoChars? b) => Equals(ref a, ref b);
    public static bool operator !=(RoChars? a, RoChars? b) => !Equals(ref a, ref b);
    #endregion Equality (RoChars?)

    #region Equality (object)
    public override bool Equals(object? obj) {
      if (obj is RoChars span) return Equals(ref this, ref span);
      if (obj is string str) return Equals(ref this, str);

      return false;
    }
    #endregion Equality (object)
  }
}